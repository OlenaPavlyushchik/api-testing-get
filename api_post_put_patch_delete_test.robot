*** Settings ***
Library  Collections
Library  RequestsLibrary
Suite Setup  Create Session  jsonplaceholder  https://jsonplaceholder.typicode.com

*** Variables ***
${data}=  Create Dictionary  userId=${1}  title=title for new chore  completed=${false}
${data_changed}=  Create Dictionary  title=sunt aut facere repellat provident
...  occaecati excepturi optio reprehenderit  body=changed body  userId=${1}  id=${1}
${data_to_patch}=  Create Dictionary  email=Changed@april.biz

*** Test Cases ***
Post New TODO
    [Documentation]  This test case sends a POST request to create a new TO-DO item for user with id 1.
    ...  It then checks that the new item has been created successfully by confirming that the response status is 201
    ...  and checking that the userId in the response matches the userId in the request
    ${response}  POST On Session  jsonplaceholder  /users/1/todos  data=${data}
    Status Should Be  201  ${response}
    Should Be Equal As Strings  ${response.reason}  Created
    Dictionary Should Contain Key  ${response.json()}  id
    Should Be Equal as Integers  ${response.json()}[userId]  1

Put Test for a Post
    [Documentation]  This test case sends a PUT request to modify a post with id 1.
    ...  It then checks that the post status is 200, confirming that the modification was successful.
    ...  It also checks that the id of the modified post is 1.
    ${response}=  PUT On Session   jsonplaceholder  /posts/1  data=${data_changed}
    Status Should Be  200  ${response}
    Should Be Equal As Strings  ${response.reason}  OK
    Should Be Equal as Integers  ${response.json()}[id]  1

Patch Test for a User
    [Documentation]  This test case sends a PATCH request to modify the email of a user with id 1.
    ...  The test case verifies that the user's email has been modified successfully by confirming that the
    ...  response status is 200 and checking that the email key is present in the response.
    ${response}=  PATCH On Session  jsonplaceholder  /users/1  data=${data_to_patch}
    Status Should Be  200  ${response}
    Should Be Equal As Strings  ${response.reason}  OK
    Should Be Equal as Strings  ${response.json()}[name]  Leanne Graham
    Dictionary Should Contain Key  ${response.json()}  email

Delete Test for an Album
    [Documentation]  This test case sends a DELETE request to delete an album with id 1.
    ...  The test case verifies that the album has been deleted successfully by confirming that the response status is
    ...  200 and checking that the response body is empty.
    ${response}=  DELETE On Session  jsonplaceholder  /albums/1
    Status Should Be  200  ${response}
    Should Be Equal As Strings  ${response.reason}  OK
    Should Be Empty    ${response.json()}


