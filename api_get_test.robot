*** Settings ***
Library  Collections
Library  RequestsLibrary
Suite Setup  Create Session  jsonplaceholder  https://jsonplaceholder.typicode.com

*** Test Cases ***
Get Specific Post Test
    [Documentation]  Test to validate GET request to /posts/1, check the status code and data returned for a specific
    ...  post
    ${resp_json}=  GET On Session  jsonplaceholder  /posts/1
    Status Should Be  200  ${resp_json}
    Should Be Equal As Strings  ${resp_json.reason}  OK
    Dictionary Should Contain Value  ${resp_json.json()}  sunt aut facere repellat provident occaecati excepturi optio reprehenderit
    Dictionary Should Contain Key  ${resp_json.json()}  id
    Should Be Equal As Strings  1  ${resp_json.json()}[id]
    Dictionary Should Contain Key  ${resp_json.json()}  userId
    Should Be Equal As Strings  1  ${resp_json.json()}[userId]
    Dictionary Should Contain Key  ${resp_json.json()}  body

Get Todos Test
    [Documentation]  Test to validate GET request to /todos, checks the status code and every key-value pair for first
    ...  todo in a list
    ${response}=  Get On Session  jsonplaceholder  /todos
    Status Should Be  200  ${response}
    Should Be Equal as Integers  ${response.json()}[0][userId]  1
    Should Be Equal as Integers  ${response.json()}[0][id]  1
    Should Be Equal as Strings  ${response.json()}[0][title]  delectus aut autem
    Should Be Equal as Strings  ${response.json()}[3][completed]  ${True}

Get Specific User Test
    [Documentation]  Test to validate GET request to /users/2, checks the status code, and data returned for specific
    ...  user
    ${user_response}=  Get On Session  jsonplaceholder  /users/2
    Status Should Be  200  ${user_response}
    Should Be Equal As Strings  ${user_response.reason}  OK
    Should Be Equal as Integers  ${user_response.json()}[id]  2
    Should Be Equal as Strings  ${user_response.json()}[name]  Ervin Howell
    Should Be Equal as Strings  ${user_response.json()}[username]  Antonette
    Should Be Equal as Strings  ${user_response.json()}[email]  Shanna@melissa.tv
    Should Be Equal as Strings  ${user_response.json()}[phone]  010-692-6593 x09125
    Should Be Equal as Strings  ${user_response.json()}[website]  anastasia.net
    Dictionary Should Contain Key  ${user_response.json()}  address
    Dictionary Should Contain Key  ${user_response.json()}  company









