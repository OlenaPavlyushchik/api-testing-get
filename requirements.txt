attrs==23.1.0
certifi==2023.11.17
charset-normalizer==3.3.2
click==8.1.7
docutils==0.20.1
flex==6.14.1
genson==1.2.2
h11==0.14.0
idna==3.6
iniconfig==2.0.0
Jinja2==3.1.2
jsonpath-ng==1.6.0
jsonpointer==2.4
jsonschema==4.20.0
jsonschema-specifications==2023.11.2
MarkupSafe==2.1.3
outcome==1.3.0.post0
packaging==23.2
page==0.2
pluggy==1.3.0
ply==3.11
Pygments==2.17.2
PySocks==1.7.1
pytest==7.4.3
pytest-env==1.1.3
pytest-html==4.1.1
pytest-metadata==3.0.0
pytz==2023.3.post1
PyYAML==6.0.1
referencing==0.32.0
requests==2.31.0
RESTinstance==1.3.0
rfc3987==1.3.8
robotframework==6.1.1
robotframework-requests==0.9.6
rpds-py==0.15.2
selenium==4.15.2
six==1.16.0
sniffio==1.3.0
sortedcontainers==2.4.0
strict-rfc3339==0.7
trio==0.23.1
trio-websocket==0.11.1
tzlocal==5.2
urllib3==2.1.0
validate-email==1.3
wsproto==1.2.0
