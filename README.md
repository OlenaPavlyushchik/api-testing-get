## Description

The repository contains tests for testing API using Robot Framework to 
validate the functionality of the GET, POST, PUT, PATCH and DELETE requests for the API endpoint: 
https://jsonplaceholder.typicode.com.

## Installation and execution
First install Robot framework:
pip install robotframework or pip3 install robotframework OR check 
that it is already installed robot --version

## Install dependencies:
```sh
pip install -r requirements.txt
```
Clone project from git to your local: select 'Clone with SSH key', copy URL
git clone 'URL'

To run tests enter in PyCharm terminal:
```sh
cd api-testing-get
robot api_get_test.robot api_post_put_patch_delete_test.robot
```
To review the report:
open report.html
select browser

